﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;


public class BundleMenu
{
	[MenuItem("AssetBundles/Clear Cache")]
	static void ClearCache()
	{
		Caching.ClearCache ();
	}
	
	[MenuItem("AssetBundles/Build for PC")]
	static void TogglePCBuild ()
	{
		EditorPrefs.SetBool("buildPC", !EditorPrefs.GetBool("buildPC", false));
	}
	[MenuItem("AssetBundles/Build for PC", true)]
	static bool TogglePCBuildValidate ()
	{
		Menu.SetChecked("AssetBundles/Build for PC", EditorPrefs.GetBool("buildPC", false));
		return true;
	}
	
	[MenuItem("AssetBundles/Build for OSX")]
	static void ToggleOSXBuild ()
	{
		EditorPrefs.SetBool("buildOSX", !EditorPrefs.GetBool("buildOSX", false));
	}
	[MenuItem("AssetBundles/Build for OSX", true)]
	static bool ToggleOSXBuildValidate ()
	{
		Menu.SetChecked("AssetBundles/Build for OSX", EditorPrefs.GetBool("buildOSX", false));
		return true;
	}
	
	[MenuItem("AssetBundles/Build for iOS")]
	static void ToggleiOSBuild ()
	{
		EditorPrefs.SetBool("buildiOS", !EditorPrefs.GetBool("buildiOS", false));
	}
	[MenuItem("AssetBundles/Build for iOS", true)]
	static bool ToggleiOSBuildValidate ()
	{
		Menu.SetChecked("AssetBundles/Build for iOS", EditorPrefs.GetBool("buildiOS", false));
		return true;
	}
	
	[MenuItem("AssetBundles/Build for Android")]
	static void ToggleAndroidBuild ()
	{
		EditorPrefs.SetBool("buildAndroid", !EditorPrefs.GetBool("buildAndroid", false));
	}
	[MenuItem("AssetBundles/Build for Android", true)]
	static bool ToggleAndroidBuildValidate ()
	{
		Menu.SetChecked("AssetBundles/Build for Android", EditorPrefs.GetBool("buildAndroid", false));
		return true;
	}

	[MenuItem("AssetBundles/Build for WebGL")]
	static void ToggleWebGLBuild ()
	{
		EditorPrefs.SetBool("buildWebGL", !EditorPrefs.GetBool("buildWebGL", false));
	}
	[MenuItem("AssetBundles/Build for WebGL", true)]
	static bool ToggleWebGLBuildValidate ()
	{
		Menu.SetChecked("AssetBundles/Build for WebGL", EditorPrefs.GetBool("buildWebGL", false));
		return true;
	}

	[MenuItem("AssetBundles/Build Asset Bundles")]
	static void BuildAssetBundles() 
	{
		// Bring up save panel
		string path = EditorUtility.SaveFolderPanel ("Save Bundle", "", "");
		if (path.Length != 0) 
		{   
			if(EditorPrefs.GetBool("buildPC", false)) 
				BuildBundle(path + "/PC", BuildTarget.StandaloneWindows);
			
			if(EditorPrefs.GetBool("buildOSX", false))
				BuildBundle(path + "/OSX", BuildTarget.StandaloneOSXUniversal);
			
			if(EditorPrefs.GetBool("buildiOS", false))
				BuildBundle(path + "/iOS", BuildTarget.iOS);
			
			if(EditorPrefs.GetBool("buildAndroid", false))
				BuildBundle(path + "/Android", BuildTarget.Android);

			if(EditorPrefs.GetBool("buildWebGL", false))
				BuildBundle(path + "/WebGL", BuildTarget.WebGL);   
		}


		/*
		Process myProc = new Process();
		myProc.StartInfo.FileName = "create_bundles.sh";
		myProc.StartInfo.UseShellExecute = true;
		myProc.StartInfo.ErrorDialog = true;
		myProc.StartInfo.WorkingDirectory = path;
		myProc.StartInfo.CreateNoWindow = true;
//		myProc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
		myProc.Start();
*/

		BuildDotBundleFile (path);

	}

	// Build .bundle file
	static void BuildDotBundleFile(string bundleBuildsPath){

		List<string> bundles = new List<string> ();
		List<string> bundlePathsAndroid = new List<string>();
		List<string> bundlePathsIOS = new List<string>();
		List<string> bundlePathsWebGL = new List<string>();

		// Check Android folder first
		DirectoryInfo info = new DirectoryInfo(bundleBuildsPath + "/Android/");
		FileInfo[] fileInfo = info.GetFiles();

		string androidPathToZip = "";

		foreach (FileInfo file in fileInfo) {
			// Check for files without extension
			if (string.IsNullOrEmpty (file.Extension)) {
				if (file.Name != "Android") {
					androidPathToZip = bundleBuildsPath + "/android_" + file.Name;
					bundlePathsAndroid.Add (androidPathToZip);
					bundles.Add (file.Name);
					// Replace old file if it exists
					if (File.Exists (androidPathToZip))
						File.Delete (androidPathToZip);
					
					File.Copy (bundleBuildsPath + "/Android/" + file.Name, androidPathToZip);
				}
			}
		}

		// Same for IOS
		DirectoryInfo iosinfo = new DirectoryInfo(bundleBuildsPath + "/iOS/");
		FileInfo[] iosfileInfo = iosinfo.GetFiles();

		string iosPathToZip = "";

		foreach (FileInfo file in iosfileInfo) {
			// Check for files without extension
			if (string.IsNullOrEmpty (file.Extension)) {
				if (file.Name != "iOS") {
					iosPathToZip = bundleBuildsPath + "/ios_" + file.Name;
					bundlePathsIOS.Add (iosPathToZip);
					// Replace old file if it exists
					if (File.Exists (iosPathToZip))
						File.Delete (iosPathToZip);
					
					File.Copy (bundleBuildsPath + "/iOS/" + file.Name, iosPathToZip);

				}
			}
		}

		// Same for WebGL
		DirectoryInfo webglinfo = new DirectoryInfo(bundleBuildsPath + "/WebGL/");
		FileInfo[] webglfileInfo = webglinfo.GetFiles();

		string webglPathToZip = "";

		foreach (FileInfo file in webglfileInfo) {
			// Check for files without extension
			if (string.IsNullOrEmpty (file.Extension)) {
				if (file.Name != "WebGL") {
					webglPathToZip = bundleBuildsPath + "/webgl_" + file.Name;
					bundlePathsWebGL.Add (webglPathToZip);
					// Replace old file if it exists
					if (File.Exists (webglPathToZip))
						File.Delete (webglPathToZip);

					File.Copy (bundleBuildsPath + "/WebGL/" + file.Name, webglPathToZip);

				}
			}
		}

		int i = 0;
		foreach (string bundleName in bundles) {
			string[] filesToZip = new string[3];

			if (!string.IsNullOrEmpty (bundlePathsAndroid[i]) && !string.IsNullOrEmpty (bundlePathsIOS[i]) && !string.IsNullOrEmpty (bundlePathsWebGL[i])) {
				filesToZip [0] = bundlePathsAndroid[i];
				filesToZip [1] = bundlePathsIOS[i];
				filesToZip [2] = bundlePathsWebGL[i];

				// Replace old file if it exists
				if (File.Exists (bundleBuildsPath + "/" + bundleName + ".bundle"))
					File.Delete (bundleBuildsPath + "/" + bundleName + ".bundle");

				ZipUtil.Zip (bundleBuildsPath + "/" + bundleName + ".bundle", filesToZip);
			}
			File.Delete (bundlePathsAndroid[i]);
			File.Delete (bundlePathsIOS[i]);
			File.Delete (bundlePathsWebGL[i]);
			i++;
		}

		Directory.Delete (bundleBuildsPath + "/iOS/", true);
		Directory.Delete (bundleBuildsPath + "/Android/", true);
		Directory.Delete (bundleBuildsPath + "/WebGL/", true);

	}
	
	static void BuildBundle(string path, BuildTarget target)
	{
		if (!Directory.Exists (path))
			Directory.CreateDirectory (path);
		
		BuildPipeline.BuildAssetBundles (path, BuildAssetBundleOptions.None, target);
	}
}