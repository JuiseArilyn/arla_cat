﻿using UnityEngine;
using System.Collections;

public class EyeDemo : MonoBehaviour {

    public GameObject realHead;
    public GameObject toonHead;
    
    private GameObject currentObject;    
    private bool toonMode = false;

    private IrisFXController iris;

    void Start () {
        updateVisibility();
	}

    void updateVisibility()
    {
        if (toonMode)
        {
            currentObject = toonHead;            
        }            
        else
        {
            currentObject = realHead;            
        }            

        realHead.GetComponent<Renderer>().enabled = !toonMode;        
        toonHead.GetComponent<Renderer>().enabled = toonMode;

        iris = currentObject.GetComponent<IrisFXController>();
        if (iris == null)
        {
            iris = currentObject.GetComponentInChildren<IrisFXController>();
        }
    }
	
	void Update ()
    {       
        Vector3 mousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        mousePos -= new Vector3(0.5f, 0.5f, 0.0f);
        

        float angle = 180.0f + mousePos.x * -90.0f;
        currentObject.transform.rotation = Quaternion.Euler(0.0f, angle, 0.0f);        

        if (iris!=null)
        {
            iris.eyelookingDir = new Vector4(mousePos.x, mousePos.y);
        }
	}

    void OnGUI()
    {
        Material targetMaterial = currentObject.GetComponent<Renderer>().material;
        if (!targetMaterial)
            return;

        GUI.Box(new Rect(10, 10, 200, 120), "IrisFX Demo");

        if (GUI.Button(new Rect(20, 40, 180, 20), "Swap Model"))
        {
            toonMode = !toonMode;
            this.updateVisibility();
        }

        if (iris == null)
        {
            return;
        }

        if (GUI.Button(new Rect(20, 70, 180, 20), "Random Color"))
        {
            iris.irisColor = new Color(Random.Range(0.5f, 1.0f), Random.Range(0.5f, 1.0f), Random.Range(0.5f, 1.0f));
        }

        if (toonMode && GUI.Button(new Rect(20, 100, 180, 20), "Random Shape"))
        {
            iris.RandomizeShape();
        }
    }
}
