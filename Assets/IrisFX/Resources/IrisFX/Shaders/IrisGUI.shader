// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "IrisFX/GUI" 
{
	Properties { _MainTex ("Texture", any) = "" {} 
	
		_IrisTex("Iris (RGB)", 2D) = "white" {}
		_IrisColor("IrisColor", Color) = (1, 1, 1,1)
		_IrisOffset("Iris Offset", Vector) = (0.0, 0.0, -0.125, -0.12)
		_IrisScale("Iris Scale", Range(0.1, 2.0)) = 1.0
		_IrisLimit("Iris Clamp Limit", Range(0.0, 0.3)) = 0.1
		_EyeShape("Eye Shape", Vector) = (0.0, 0.3, 0.4, 0.1)
		_EyeThicknessTop("Eye Lid Thickness (Top)", Range(0, 12)) = 4.0
		_EyeThicknessBottom("Eye Lid Thickness (Bottom)", Range(0, 12)) = 2.5
		_EyeLidColor("Eye Lid Color", Color) = (0,0,0,1)
		_EyeShadowStrength("Eye Shadow Strength", Range(0, 1)) = 0.75
		_EyeDistance("Eye Separation", Range(0.1, 0.35)) = 0.15
		_EyeSetup("Eye Offset/Scale", Vector) = (0.5, 0.5, 0.3, 0.3)
		_BlinkOffset("Blink Offset", Float) = 0
		[Toggle] _EyeBorders("Eye Borders", Float) = 0
		[Toggle] _EyeShine("Eye Shine", Float) = 0
		[Toggle] _EyeOcclusion("Eye Occlusion", Float) = 0
	}

	SubShader {

		Tags { "ForceSupported" = "True" "RenderType"="Overlay" } 
		
		Lighting Off 
		Blend SrcAlpha OneMinusSrcAlpha 
		Cull Off 
		ZWrite Off 
		ZTest Always 
		
		Pass {	
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

/*
#pragma multi_compile _EYESHINE_ON _EYESHINE_OFF 
#pragma multi_compile _EYEBORDERS_ON _EYEBORDERS_OFF 
#pragma multi_compile _EYEOCCLUSION_ON _EYEOCCLUSION_OFF 			
*/

			#include "UnityCG.cginc"

			#include "IrisFX.cginc"

			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			sampler2D _MainTex;

			uniform float4 _MainTex_ST;
			
			v2f vert (appdata_t v)
			{				
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{				
				fixed4 baseColor = tex2D(_MainTex, i.texcoord);
				EyeOutput eyeValue = traceEye(i.texcoord, baseColor);
				baseColor = eyeValue.color;

				return 2.0f * baseColor * i.color;
			}
			ENDCG 
		}
	} 
	
	Fallback off 
}
