﻿Shader "IrisFX/Toon" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}		
		_Ramp("Toon Ramp", 2D) = "white" {}
		_IrisTex ("Iris (RGB)", 2D) = "white" {}
		_IrisColor ("IrisColor", Color) = (1, 1, 1,1)
		_IrisOffset ("Iris Offset", Vector) = (0.0, 0.0, -0.125, -0.12)
		_IrisScale("Iris Scale", Range(0.1, 2.0)) = 1.0
		_IrisLimit("Iris Clamp Limit", Range(0.0, 0.3)) = 0.1		
		_EyeShape ("Eye Shape", Vector) = (0.0, 0.3, 0.4, 0.1)				
		_EyeThicknessTop ("Eye Lid Thickness (Top)", Range(0, 12)) = 4.0
		_EyeThicknessBottom ("Eye Lid Thickness (Bottom)", Range(0, 12)) = 2.5
		_EyeLidColor ("Eye Lid Color", Color) = (0,0,0,1)		
		_EyeShadowStrength ("Eye Shadow Strength", Range(0, 1)) = 0.75
		_EyeDistance ("Eye Separation", Range(0.1, 0.35)) = 0.15
		_EyeSetup ("Eye Offset/Scale", Vector) = (0.5, 0.5, 0.3, 0.3)		
		_BlinkOffset("Blink Offset", Float) = 0
		[Toggle] _EyeBorders("Eye Borders", Float) = 0
		[Toggle] _EyeShine("Eye Shine", Float) = 1
		[Toggle] _EyeOcclusion("Eye Occlusion", Float) = 0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
			
		CGPROGRAM
		#pragma surface surf CelShadingForward fullforwardshadows

#pragma multi_compile _EYESHINE_ON _EYESHINE_OFF 
#pragma multi_compile _EYEBORDERS_ON _EYEBORDERS_OFF 
#pragma multi_compile _EYEOCCLUSION_ON _EYEOCCLUSION_OFF 			

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		sampler2D _Ramp;
		fixed4 _Color;
		
		#include "IrisFX.cginc"

		half4 LightingCelShadingForward(SurfaceOutput s, half3 lightDir, half atten)
		{
			half NdotL = dot(s.Normal, lightDir);

			NdotL = NdotL * 0.5 + 0.5;
			//NdotL *= atten;
			
			half3 ramp = tex2D(_Ramp, half2(NdotL, 0.5)).rgb;

			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * ramp * atten;
			c.a = 1.0; // s.Alpha;
			return c;
		}
		
		void surf(Input IN, inout SurfaceOutput o) {		
			// Albedo comes from a texture tinted by color
			fixed4 baseColor = tex2D (_MainTex, IN.uv_MainTex) * _Color;			
			EyeOutput eyeValue = traceEye(IN.uv_MainTex, baseColor);
			fixed4 eyeColor = eyeValue.color;			
			o.Albedo = eyeColor.rgb;
			

			//o.Specular = eyeColor.a;
			o.Alpha = 1.0;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
