﻿sampler2D _IrisTex;
fixed iResolution;
fixed4 _IrisOffset;
float _IrisScale;
float _IrisLimit;
fixed4 _IrisColor;
fixed4 _EyeShape;
half _EyeThicknessTop;
half _EyeThicknessBottom;
half _EyeDistance;
half _EyeShadowStrength;
fixed4 _EyeSetup;
fixed4 _EyeLidColor;
float _BlinkOffset;

float det(float2 a, float2 b) { return a.x*b.y-b.x*a.y; }

// From: http://research.microsoft.com/en-us/um/people/hoppe/ravg.pdf
float2 get_distance_vector(float2 b0, float2 b1, float2 b2) {	
  float a=det(b0,b2), b=2.0*det(b1,b0), d=2.0*det(b2,b1); 		
  float f = b*d-a*a; 
  float2 d21=b2-b1, d10=b1-b0, d20=b2-b0;
  float2 gf=2.0*(b*d21+d*d10+a*d20);
  gf = float2(gf.y,-gf.x);
  float2 pp = -f*gf/dot(gf,gf); 
  float2 d0p = b0-pp;
  float ap = det(d0p,d20), bp=2.0*det(d10,d0p); 
  // (note that 2*ap+bp+dp=2*a+b+d=4*area(b0,b1,b2))
  float t=clamp((ap+bp)/(2.0*a+b+d), 0.0 ,1.0);
  return lerp(lerp(b0,b1,t),lerp(b1,b2,t),t); 
}

float approx_distance(float2 p, float2 b0, float2 b1, float2 b2) 
{
  return length(get_distance_vector(b0-p, b1-p, b2-p));
}

float insideCurve(float2 A, float2 B, float2 C, float2 pixelPos, float thickness)
{
	
	float2 mult = iResolution;
	
	pixelPos *= mult;
	float2 b0 = A * mult;
	float2 b1 = B * mult;
	float2 b2 = C * mult;
	float2 mid = 0.5*(b0+b2) + float2(0.0, 0.01);
	
	float d = approx_distance(pixelPos, b0, b1, b2);

	// Anti-alias the edge.
	return step(thickness, d) * smoothstep(d, thickness, thickness+0.5);
}

float insideSemiBlob(float2 A, float2 B, float2 C, float2 pixelPos)
{
	// Compute vectors        
	float2 v0 = C - A;
	float2 v1 = B - A;
	float2 v2 = pixelPos - A;

	// Compute dot products
	float dot00 = dot(v0, v0);
	float dot01 = dot(v0, v1);
	float dot02 = dot(v0, v2);
	float dot11 = dot(v1, v1);
	float dot12 = dot(v1, v2);

	// Compute barycentric coordinates
	float invDenom = 1.0 / (dot00 * dot11 - dot01 * dot01);
	float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
	float v = (dot00 * dot12 - dot01 * dot02) * invDenom;

	// use the blinn and loop method
	float w = (1.0 - u - v);
	float2 textureCoord = u * float2(0.0,0.0) + v * float2(0.5,0.0) + w * float2(1.0,1.0);
		
	// use the sign of the result to test if inside or outside
	float insideOutside = sign(textureCoord.x * textureCoord.x - textureCoord.y) < 0.0 ? 1.0 : 0.0;
	
	
	// check if it's outside the triangle
	insideOutside *= step(0.0, v);
	//insideOutside *= ((u >= 0.0) && (v >= 0.0) && (u + v < 1.0)) ? 1.0 : 0.0;
							   
	return insideOutside;
}


float insideBlob(float2 A, float2 B, float2 C, float2 D, float2 pixelPos)
{
	float vA = insideSemiBlob(A, B, C, pixelPos);
	float vB = insideSemiBlob(A, D, C, pixelPos);
	
	return max(vA, vB);  
}
	
struct EyeOutput {
	half4 color;
	
	#ifdef EYE_NORMALS
	half3 normal;
	#endif
};


EyeOutput traceEye(half2 baseUV, float4 colorBG)
{	
	iResolution = 800;

	float2 eyeScale = _EyeSetup.zw;
					
	float2 pixelPos = baseUV - _EyeSetup.xy;	
	fixed2 irisMult = fixed2(2.0, 2.0);
	float2 irisOffset = clamp(_IrisOffset.xy, -_IrisLimit, _IrisLimit);

	if (pixelPos.x>0.5)
	{
		pixelPos.x = 1.0 - pixelPos.x;
		irisMult.x *= -1.0;
		irisOffset.x = -irisOffset.x;		
	}
		
	float2 eyeOffset = float2(0.5 - _EyeDistance, 0.5);	  
							
	pixelPos -= eyeOffset;	
	pixelPos /= eyeScale;
	
	float irisScale = 1.0 / _IrisScale;
							
	float2 irisDist = pixelPos - irisOffset;
	float2 irisUV = irisDist * irisMult + 0.5;
	fixed4 irisColor = tex2D(_IrisTex, 0.5 + (irisUV -0.5) * irisScale);
	
	irisColor = lerp(irisColor, _IrisColor * irisColor, irisColor.a);
	
	
	float4 outColor = float4(0.0, 0.0, 0.0, 0.0);		
	float4 colorWhite  = float4(1.0, 1.0, 1.0, 1.0);
				
	//float blinkScale = 0.001 + 0.99 * smoothstep(0.1, 0.25, abs(_SinTime.w + _BlinkOffset) * 2.0);
	float blinkScale = 0.001 + min(0.999, _BlinkOffset);
		
	float2 A = float2(-_EyeShape.z, _EyeShape.w * blinkScale);
	float2 B = float2(_EyeShape.x, _EyeShape.y * blinkScale);
	float2 C = float2(_EyeShape.z, 0.0);      
	float2 D = float2(0.0, -_EyeShape.y * blinkScale);
	float insideEye = insideBlob(A, B, C, D, pixelPos);
	outColor = lerp(colorBG, irisColor, insideEye);
		

	float insideLid = min(insideCurve(A, B, C, pixelPos, _EyeThicknessTop), insideCurve(A, D, C, pixelPos, _EyeThicknessBottom));
	outColor = lerp(_EyeLidColor, outColor, insideLid);

				
	// add shadow from top eye lid 
	A = float2(-_EyeShape.z * 2.0, -_EyeShape.y);
	B = float2(_EyeShape.x * 1.75, _EyeShape.y * 1.75);
	C = float2(_EyeShape.z * 2.0, -_EyeShape.y);      
	float insideShadow = insideSemiBlob(A, B, C, pixelPos);			
	half shadowValue = lerp(1.0, _EyeShadowStrength, step(1.0, insideEye * (1.0 - insideShadow)));
							
	// add slight occlusion effect around eye
#if _EYEOCCLUSION_ON
	float r = sqrt(dot(pixelPos, pixelPos)) * 3.25;
	float eyeFade = smoothstep(0.5, 0.95, r);
	shadowValue = min(shadowValue, 1.0 - (0.5 * eyeFade *insideEye));
#endif
			
	outColor *= shadowValue; 
	
	// add shine to iris (not affected by shadows)
#if _EYESHINE_ON
	float eyeShine = 1.0 - smoothstep(0.0, 0.35 * eyeScale.x, length(irisDist + eyeScale * irisMult * _IrisOffset.wz));
	outColor.rgb += half3(1.0, 0.9, 0.8) * eyeShine * 0.9 * insideEye;     
#endif
	
	//outColor =  eyeColor  *abs(_CosTime.w);		
	//outColor = float4(pixelPos.x, pixelPos.y, 0.0, 1.0);

	#ifdef EYE_NORMALS
		float2 tempUV = pixelPos.xy * irisMult * 2.0 + 0.5;
		
        float uv = tempUV.x * tempUV.y;
        float oneMinusU = 1.0 - tempUV.x;
        float oneMinusV = 1.0 - tempUV.y;
        float uOneMinusV = tempUV.x * oneMinusV;
        float vOneMinusU = tempUV.y * oneMinusU;
        float oneMinusUOneMinusV = oneMinusU * oneMinusV;
		
		float3 topLeft = float3(1.0, 1.0, 0.0) * 2.0 - 1.0;
		float3 topRight = float3(0.0, 1.0, 1.0) * 2.0 - 1.0;
		float3 bottomLeft = float3(1.0, 0.0, 1.0) * 2.0 - 1.0;
		float3 bottomRight = float3(0.0, 0.0, 1.0) * 2.0 - 1.0;
		        
		half3 sphere_vec = normalize(
			topLeft * oneMinusUOneMinusV + 
			topRight * uOneMinusV + 
			bottomLeft * vOneMinusU + 
			bottomRight * uv
		);		
	#endif
	
	EyeOutput result;
	result.color = outColor;
	
	#ifdef EYE_NORMALS
	result.normal = lerp(half3(0.0, 0.0, 1.0), sphere_vec, insideEye);
	#endif
		
	return result;
}
