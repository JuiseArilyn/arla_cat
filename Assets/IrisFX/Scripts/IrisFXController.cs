﻿using UnityEngine;
using System.Collections.Generic;

[AddComponentMenu("IrisFX/IrisFXController")]
[System.Serializable]
public class IrisFXController : MonoBehaviour {

    public enum EyeStyle
    {
        Toon,
        Realistic,
        Custom
    }

    [SerializeField]
    public EyeStyle eyeStyle;

    [SerializeField]
    public Shader shader;

    [SerializeField]
    public Texture irisTexture;

    [SerializeField]
    public Color irisColor = Color.white;

    [SerializeField]
    public float irisScale = 1.0f;

    [SerializeField]
    public float irisLimit = 0.1f;

    [SerializeField]
    public Vector2 eyeOffset = new Vector2(0, 0);

    [SerializeField]
    public Vector2 eyeScale = new Vector2(0.5f, 0.5f);

    [SerializeField]
    public Vector4 eyeShape = new Vector4(0, 0.3f, 0.4f, 0.1f);

    [SerializeField]
    public float eyeThicknessTop = 4.0f;

    [SerializeField]
    public float eyeThicknessBottom = 2.5f;

    [SerializeField]
    public Color eyeLidColor = Color.black;

    [SerializeField]
    public bool hasEyeBorders = true;

    [SerializeField]
    public float eyeShadowStrength = 0.25f;

    [SerializeField]
    public float eyeDistance = 0.15f;

    [SerializeField]
    public Vector2 eyelookingDir = new Vector2(0, 0);

    [SerializeField]
    public bool hasEyeShine = true;

    [SerializeField]
    public Vector2 shineOffset = new Vector2(0.3f, 0.3f);

    [SerializeField]
    public bool hasEyeOcclusion = true;

    [SerializeField]
    public bool hasBlink = true;

    [SerializeField]
    public float blinkFrequency = 0.2f;

    [SerializeField]
    public float blinkDuration = 0.1f;

    [SerializeField]
    public GameObject lookTarget = null;

    [SerializeField]
    public float eyeReactionTime = 0.25f;

    private Material _baseMaterial;
    private Material _irisMaterial;

    public Material irisMaterial
    {
        get { return _irisMaterial; }
    }

    public Material baseMaterial
    {
        get
        {
            if (_baseMaterial == null)
            {
                if (targetRenderer == null)
                {
                    targetRenderer = this.GetComponent<Renderer>();
                }

                if (targetRenderer == null)
                {
                    return null;
                }
                return targetRenderer.sharedMaterial;
            }
            return _baseMaterial;
        }
    }

    public void ApplyPropertiesToMaterial(Material mat)
    {
        mat.SetTexture("_IrisTex", this.irisTexture);
        mat.SetColor("_IrisColor", this.irisColor);

        mat.SetVector("_IrisOffset", new Vector4(eyelookingDir.x * 0.5f, eyelookingDir.y * 0.5f, shineOffset.x, shineOffset.y));
        
        mat.SetFloat("_IrisScale", this.irisScale);
        mat.SetFloat("_IrisLimit", this.irisLimit);
        mat.SetVector("_EyeShape", this.eyeShape);
        mat.SetFloat("_EyeThicknessTop", this.eyeThicknessTop);
        mat.SetFloat("_EyeThicknessBottom", this.eyeThicknessBottom);
        mat.SetColor("_EyeLidColor", this.eyeLidColor);

        mat.SetInt("_EyeBorders", this.hasEyeBorders ? 1 : 0);
        if (this.hasEyeBorders)
        {
            mat.EnableKeyword("_EYEBORDERS_ON");
            mat.DisableKeyword("_EYEBORDERS_OFF");
        }
        else
        {
            mat.EnableKeyword("_EYEBORDERS_OFF");
            mat.DisableKeyword("_EYEBORDERS_ON");
        }

        mat.SetInt("_EyeShine", this.hasEyeShine ? 1 : 0);
        if (this.hasEyeShine)
        {
            mat.EnableKeyword("_EYESHINE_ON");
            mat.DisableKeyword("_EYESHINE_OFF");
        }
        else
        {
            mat.EnableKeyword("_EYESHINE_OFF");
            mat.DisableKeyword("_EYESHINE_ON");
        }


        mat.SetInt("_EyeOcclusion", this.hasEyeOcclusion ? 1 : 0);
        if (this.hasEyeOcclusion)
        {
            mat.EnableKeyword("_EYEOCCLUSION_ON");
            mat.DisableKeyword("_EYEOCCLUSION_OFF");
        }
        else
        {
            mat.EnableKeyword("_EYEOCCLUSION_OFF");
            mat.DisableKeyword("_EYEOCCLUSION_ON");
        }

        mat.SetFloat("_EyeShadowStrength", 1.0f - this.eyeShadowStrength);
        mat.SetFloat("_EyeDistance", this.eyeDistance);
        mat.SetVector("_EyeSetup", new Vector4(this.eyeOffset.x, this.eyeOffset.y, this.eyeScale.x, this.eyeScale.y));

        float blinkTime = 1;
        if (hasBlink)
        {
            float tt;
#if UNITY_EDITOR
            tt = (float)System.Environment.TickCount / 1000.0f;
#else
        tt = Time.time;
#endif

            tt += _BlinkOffset;
            tt *= this.blinkFrequency;
            

            float limit = 1.0f - this.blinkDuration;
            tt = tt - Mathf.Floor(tt);
            if (tt >= limit)
            {
                tt = (tt - limit) / (1.0f - limit);
                blinkTime = Mathf.Abs(Mathf.Cos(180.0f * Mathf.Deg2Rad * tt));
            }
        }

        mat.SetFloat("_BlinkOffset", blinkTime);
    }

    public void RandomizeShape()
    {
        this.eyeShape = new Vector4(Random.Range(-0.5f, 0.4f), Random.Range(0.1f, 0.4f), Random.Range(0.3f, 0.4f), Random.Range(0.1f, 0.2f));
    }

    private Renderer targetRenderer;

    private float _BlinkOffset;

    // Use this for initialization
    void Start () {
        _BlinkOffset = Random.Range(0, 5);

        targetRenderer = this.GetComponent<Renderer>();
        if (targetRenderer != null)
        {
            Shader shader;

            if (this.eyeStyle!= EyeStyle.Custom)
            {
                shader = Shader.Find("IrisFX/"+ this.eyeStyle.ToString());
            }
            else
            {
                shader = this.shader;
            }

            _baseMaterial = targetRenderer.sharedMaterial;
            Material targetMaterial = new Material(shader);
            targetMaterial.name = "IrisFX." + this.eyeStyle.ToString();
            targetMaterial.hideFlags = HideFlags.HideAndDontSave;
            targetMaterial.CopyPropertiesFromMaterial(_baseMaterial);
            targetRenderer.sharedMaterial = targetMaterial;
        }        
	}

    float SignedAngle(Vector3 a, Vector3 b, Vector3 n)
    {
        // angle in [0,180]
        float angle = Vector3.Angle(a, b);
        float sign = Mathf.Sign(Vector3.Dot(n, Vector3.Cross(a, b)));

        // angle in [-179,180]
        float signed_angle = angle * sign;

        // angle in [0,360] (not used but included here for completeness)
        //float angle360 =  (signed_angle + 180) % 360;

        return signed_angle;
    }

    private float eyeSpeed = 0;

    void Update () {
        if (targetRenderer == null)
        {
            return;
        }

        if (lookTarget != null)
        {
            var tA = this.transform;
            var tB = lookTarget.transform;

            var dir = (tA.position - tB.position).normalized;
            float angleHorizontal = -Vector3.Dot(dir, tA.forward);

            float maxAngle = 0.85f;

            float targetDir;

            if  (angleHorizontal >= maxAngle)
            {
                angleHorizontal -= maxAngle;
                angleHorizontal /= (float)(1.0 - maxAngle);

                var relativePoint = tA.InverseTransformPoint(tB.position);
                if (relativePoint.x < 0.0) // check if in left side
                {
                    targetDir = Mathf.Lerp(0.5f, 0, angleHorizontal);
                }
                else
                {
                    targetDir = Mathf.Lerp(-0.5f, 0, angleHorizontal);
                }
            }
            else
            {
                targetDir = 0;
            }

            eyelookingDir.x = Mathf.SmoothDamp(eyelookingDir.x, targetDir, ref eyeSpeed, eyeReactionTime);
        }

        Material targetMaterial = targetRenderer.sharedMaterial;
        if (targetMaterial!=null)
        {
            this.ApplyPropertiesToMaterial(targetMaterial);
        }
	}
}
