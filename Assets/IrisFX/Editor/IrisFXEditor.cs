﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(IrisFXController))]
public class IrisFXEditor : Editor
{
    private Material irisGUI = null;

    private string[] _toolbarChoices = new string[] { "Setup", "Shape", "Eye Lids", "Other" };
    private int _toolbarSelection = 0;

    public override void OnInspectorGUI()
    {
        if (irisGUI == null)
        {
            Shader shader = Shader.Find("IrisFX/GUI");
            irisGUI = new Material(shader);
            irisGUI.hideFlags = HideFlags.HideAndDontSave;
        }

        IrisFXController iris = (IrisFXController)target;

        _toolbarSelection = GUILayout.Toolbar(_toolbarSelection, _toolbarChoices);

        if (_toolbarSelection == 0)
        {
            iris.eyeStyle = (IrisFXController.EyeStyle)EditorGUILayout.EnumPopup("Eye Style", iris.eyeStyle);
            //speed =

            if (iris.eyeStyle == IrisFXController.EyeStyle.Custom)
            {
                iris.shader = (Shader)EditorGUILayout.ObjectField("Shader", iris.shader, typeof(Shader), false);
            }

            iris.irisTexture = (Texture)EditorGUILayout.ObjectField("Iris Texture", iris.irisTexture, typeof(Texture), false);
            iris.irisColor = EditorGUILayout.ColorField("Iris Color", iris.irisColor);
            iris.irisScale = EditorGUILayout.Slider("Iris Scale", iris.irisScale, 0.1f, 9.0f);
            //float irisLimit = 0.1f;

            iris.eyeOffset.x = EditorGUILayout.Slider("Eye Horizontal Offset", iris.eyeOffset.x, -0.5f, 0.5f);
            iris.eyeOffset.y = EditorGUILayout.Slider("Eye Vertical Offset", iris.eyeOffset.y, -0.5f, 0.5f);
            iris.eyeScale.x = EditorGUILayout.Slider("Eye Horizontal Scale", iris.eyeScale.x, -0.3f, 2);
            iris.eyeScale.y = EditorGUILayout.Slider("Eye Vertical Scale", iris.eyeScale.y, -0.3f, 2);

            iris.eyeDistance = EditorGUILayout.Slider("Eye Separation", iris.eyeDistance, 0, 1);
        }

        if (_toolbarSelection == 1)
        {
            iris.eyeShape.x = EditorGUILayout.Slider("Eye Curve", iris.eyeShape.x, -0.5f, 0.9f);
            iris.eyeShape.y = EditorGUILayout.Slider("Eye Open", iris.eyeShape.y, -0.5f, 0.9f);
            iris.eyeShape.z = EditorGUILayout.Slider("Eye Width", iris.eyeShape.z, -0.3f, 0.6f);
            iris.eyeShape.w = EditorGUILayout.Slider("Eye Slant", iris.eyeShape.w, -0.5f, 0.4f);

            if (GUILayout.Button("Randomize"))
            {
                iris.RandomizeShape();
            }
        }

        if (_toolbarSelection == 2)
        {
            iris.eyeShadowStrength = EditorGUILayout.Slider("Shadow Strength", iris.eyeShadowStrength, 0, 1);
            iris.hasEyeOcclusion = EditorGUILayout.Toggle("Eye Occlusion", iris.hasEyeOcclusion);

            iris.hasEyeBorders = EditorGUILayout.Toggle("Eye Borders", iris.hasEyeBorders);
            if (iris.hasEyeBorders)
            {
                iris.eyeLidColor = EditorGUILayout.ColorField("Eyelid Color", iris.eyeLidColor);
                iris.eyeThicknessTop = EditorGUILayout.Slider("Thickness Top", iris.eyeThicknessTop, 0, 12);
                iris.eyeThicknessBottom = EditorGUILayout.Slider("Thickness Bottom", iris.eyeThicknessBottom, 0, 12);                
            }

        }

        if (_toolbarSelection == 3)
        {
            iris.lookTarget = (GameObject) EditorGUILayout.ObjectField("Look Target", iris.lookTarget, typeof(GameObject), true);
            if (iris.lookTarget == null)
            {
                iris.eyelookingDir.x = EditorGUILayout.Slider("Look Horizontal", iris.eyelookingDir.x, -1, 1);
                iris.eyelookingDir.y = EditorGUILayout.Slider("Look Vertical", iris.eyelookingDir.y, -1, 1);
            }
            else
            {
                iris.eyeReactionTime = EditorGUILayout.Slider("Reaction Time", iris.eyeReactionTime, 0.1f, 0.5f);
            }

            iris.irisLimit = EditorGUILayout.Slider("Looking Limit", iris.irisLimit, 0.0f, 0.2f);

            iris.hasEyeShine = EditorGUILayout.Toggle("Eye Shine", iris.hasEyeShine);
            if (iris.hasEyeShine)
            {
                iris.shineOffset.y = EditorGUILayout.Slider("Shine Horizontal Offset", iris.shineOffset.y, -0.5f, 0.5f);
                iris.shineOffset.x = EditorGUILayout.Slider("Shine Vertical Offset", iris.shineOffset.x, -0.5f, 0.5f);
            }

            iris.hasBlink = EditorGUILayout.Toggle("Eye Blink", iris.hasBlink);
            if (iris.hasBlink)
            {
                iris.blinkFrequency = EditorGUILayout.Slider("Blink Frequency", iris.blinkFrequency, 0.1f, 0.5f);
                iris.blinkDuration = EditorGUILayout.Slider("Blink Duration", iris.blinkDuration, 0.05f, 0.5f);
            }

        }

        if (iris.baseMaterial!=null)
        {
            float texWidth = 256;
            float texHeight = 256;

            iris.ApplyPropertiesToMaterial(irisGUI);

            Texture baseTexture = iris.baseMaterial.mainTexture != null ? iris.baseMaterial.mainTexture  : EditorGUIUtility.whiteTexture;

            Rect rect = GUILayoutUtility.GetRect(texWidth, texHeight);
            Graphics.DrawTexture(rect, baseTexture, irisGUI);
        }
        else
        {
            EditorGUILayout.LabelField("Preview", "Base material not found...");
        }
        
    }
}